import bankAccount.Account;
import bankAccount.MonetaryAmount;

public class Main {
    public static void main(String[] args) {

        System.out.println("Hello");
        Account accountObelix = new Account("sesterces", "Obelix");
        System.out.println(accountObelix.getCurrentBalance());

        //Deposer 500.5 sesterces
        accountObelix.deposit(500.5);
        System.out.println(accountObelix.getCurrentBalance());

        //Retirer 125.25 sesterces
        accountObelix.withdraw(125.25);
        System.out.println(accountObelix.getCurrentBalance());

        //Retirer 1337.0 sesterces
        accountObelix.withdraw(1337.0);
        System.out.println(accountObelix.getCurrentBalance());
    }
}
