package bankAccount;

import java.lang.Double;

public class Account {
    private MonetaryAmount balance; // qte d'argent dispo
    private final String ownerName; // nom proprietaire
    protected final String currency;

    public Account(String currency, String ownerName) {
        this.ownerName = ownerName;
        this.balance = new MonetaryAmount(0.0, currency);
        this.currency = currency;
    }

    public void deposit(double amount) {
        balance.addAmount(amount);
    }

    public void withdraw(double amount) {
        if (Double.compare(amount, balance.getAmount()) < 0) {
            balance.substractAmount(amount);
        }
    }

    public MonetaryAmount getCurrentBalance() {
        return new MonetaryAmount(balance.getAmount(), currency);
    }

    public String getOwnerName() {
        return ownerName;
    }
}

