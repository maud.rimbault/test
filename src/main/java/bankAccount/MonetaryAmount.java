package bankAccount;

public class MonetaryAmount {
    private double amount; // quantite d'argent
    private final String currency;

    public MonetaryAmount(double amount, String currency){
        this.amount = amount; // montant initial
        this.currency = currency;
    }

    public void addAmount(double a){
        this.amount += a;
    }

    public void substractAmount(double a){
        this.amount -= a;
    }

    @Override
    public String toString() {
        return "Le montant disponible sur le compte est : " + getAmount() + " " + currency ;
//        return "MonetaryAmount{" +
//                "amount=" + getAmount() +
//                ", currency='" + currency + '\'' +
//                '}';
    }

    public double getAmount() {
        return amount;
    }
}
