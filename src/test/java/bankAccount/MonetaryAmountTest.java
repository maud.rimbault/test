package bankAccount;

import org.junit.jupiter.api.*;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class MonetaryAmountTest {
    MonetaryAmount monetaryAmount = new MonetaryAmount(500.5, "euros");

    @BeforeAll
    static void initAll() {
        System.out.println("Lancement des tests");
    }

    @BeforeEach
    void init() {
        System.out.println("Lancement d'un test");
    }

    @Test
    void addAmountTest() {
        monetaryAmount.addAmount(100);
        assertEquals(600.5,monetaryAmount.getAmount());
    }

    @Test
    void substractAmountTest() {
        monetaryAmount.substractAmount(100);
        assertEquals(400.5,monetaryAmount.getAmount());
    }

    @AfterEach
    void tearDown() {
        System.out.println("Fin du test");
    }

    @AfterAll
    static void tearDownAll() {
        System.out.println("Fin des tests");
    }
}
